//**************Program done by Amit Gohel-921536726 & Purabkumar Upadhyay-921477606    ****************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Defining structure for process and queue
typedef struct proc{ 
    int pid;            
    int time;           
    int init_t;   
    struct proc *next;   
}proc;

//Create new queue for each process
proc *n_process(int pid, int time){    
    proc *new = malloc(sizeof(proc)); //memory size of new process
    new->pid = pid;                         
    new->time = time;                       
    new->init_t = time;                   
    new->next = NULL;                      
    return new;
}

typedef struct queue{   
    proc *front;     
    char *name;        
}queue;

queue *new_proc_queue(char *name){         // new queue added       
    queue *new = malloc(sizeof(queue));     
    new->front = NULL;                      //set null
    new->name = name;                       
    return new;
}

int process_tot = 0;


void enqueue(queue *queue_x, proc *p){ 	
    if(queue_x->front == NULL){  		
        p->next = NULL;  		// set the next process NULL
        queue_x->front = p;  			
        return;
    }
    else{   				
        proc *c = queue_x->front;         	// looking last process in queue  
        while(c->next != NULL){
            c = c->next;
        }
        p->next = NULL;                     
        c->next = p;      
    }
}
void init(queue *queue_x, int n){ 		
    int t;
    int i;
    for(i = 1; i <= n; i ++){
        scanf("%d", &t);		
        proc *p = n_process(process_tot, t);	// create new process which pid=process_tot, time=t
        process_tot ++;                     	
        enqueue(queue_x, p);                  // Push to Q process           
    }
}
proc* dequeue(queue *queue_x){ 		// remove first
    if(queue_x->front == NULL) return NULL;  	
    proc* c = queue_x->front;		
    queue_x->front = queue_x->front->next; 		
    return c;
}

//function for displaying all queue
void print_queue(queue *queue_x){
    printf("%s->", queue_x->name);
    proc *c = queue_x->front;                  
    while(c != NULL){
        printf("P%d(%d)->",c->pid, c->time);
        c = c->next;
    }
    printf("\n");			//display all from front 
}

int main(int argc, char * argv[]){
	// Creates three queues
    queue *Q0 = new_proc_queue("Q0");
	queue *Q1 = new_proc_queue("Q1");
	queue *Q2 = new_proc_queue("FCFS"); 
    
    int n0, n1, n2;
    scanf("%d", &n0);init(Q0, n0);           
    scanf("%d", &n1);init(Q1, n1);
    scanf("%d", &n2);init(Q2, n2);

    print_queue(Q0);           //Display all queues 
    print_queue(Q1);
    print_queue(Q2);

    int curr_t = 0;          // Current time from first
    int comp_time = 0;      // Completion time
    int waiting_time = 0;    // Waiting time

    int count = 0;                

    while(Q0->front != NULL || Q1->front != NULL || Q2->front != NULL){     //all queues are not empty
        if(Q0->front != NULL){                                              
            proc *c = dequeue(Q0);                                           
            printf("P%d(%d)\n", c->pid, c->time);
            if(c->time > 8){      //Time quontam slice 8ms                                          
                c->time -= 8;                                               
                curr_t += 8;                                         
                enqueue(Q1, c);                                              
            }
            else{                                                           
                curr_t += c->time;                                    
                c->time = 0;
                 comp_time += curr_t;   // add sum of completion time by current time
                waiting_time += curr_t - c->init_t;  // waiting_time = completion_time - burst_time 
            }
            print_queue(Q0);                                                
            print_queue(Q1);
            print_queue(Q2);
        }
		else if(Q1->front != NULL){       //Q1 is not empty but Q0 is//
            proc *c = dequeue(Q1);        
            printf("P%d(%d)\n", c->pid, c->time);
            if(c->time > 16){   //Time quontam slice 16ms                                           
				c->time -= 16; 
                curr_t += 16;  
                if(count % 4 == 0){ //Here, 25% chance that process will be promoted   
                    enqueue(Q0, c);    
                    printf("Promote\n");
                }
                else{                                                      
                    enqueue(Q2, c);                                            
                }
                count ++;                                                 
            }
            else{
                curr_t += c->time;           
                c->time = 0;
                 comp_time += curr_t;
                waiting_time += curr_t - c->init_t;
            }
            print_queue(Q0);
            print_queue(Q1);
            print_queue(Q2);
        }
	else if(Q2->front != NULL){            // If Q2 is not empty, others are empty
            proc *c = dequeue(Q2);         
            printf("P%d(%d)\n", c->pid, c->time);
            print_queue(Q0);
            print_queue(Q1);
            print_queue(Q2);
            curr_t += c->time;   // calculate current_time,  completion_time, waiting_time
            c->time = 0;
            comp_time += curr_t;
            waiting_time += curr_t - c->init_t;
        }
    }
	float average_turnaround_time = 1.0 *  comp_time / process_tot;
    float average_wait_time = 1.0 * waiting_time / process_tot;
    printf("Average wait time: %.6f\n", average_wait_time); // Calculate Average Waiting time with 6 decimal digits
    printf("Average turnarount time: %.6f", average_turnaround_time);// Calculate Turnaround time with 6 decimal digits
return 0;}
